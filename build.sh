#!/bin/bash

CC=gcc
CFLAGS="-Wall -fsanitize=address -g -std=c17"
LIBS="-lc -lm"
APPNAME="calc2"
PROJECT_ROOT=$(dirname $0)
[ -d $PROJECT_ROOT/bin ] || mkdir $PROJECT_ROOT/bin

$CC $CFLAGS -o $PROJECT_ROOT/bin/$APPNAME $PROJECT_ROOT/src/*.c $LIBS
