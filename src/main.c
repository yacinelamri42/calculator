#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "token.h"

typedef int Error;
Error calculate(TokenArray array, double* value);

Error calculate_parenthesis(TokenArray array, TokenArray* new_array) {
	TokenArray new_arr = token_array_new();
	if (!new_arr.tokens) {
		return 1;
	}
	for (int i=0; i<array.number_of_tokens; i++) {
		if (array.tokens[i].token_type == TOKEN_LEFT_PARENTHESIS) {
			int pos = i;
			i++;
			TokenArray in_parenthesis = token_array_new();
			if (!new_arr.tokens) {
				return 1;
			}
			int nested = 0;
			for (; i<array.number_of_tokens; i++) {
				if (array.tokens[i].token_type == TOKEN_LEFT_PARENTHESIS) {
					nested++;
				}
				if (array.tokens[i].token_type == TOKEN_RIGHT_PARENTHESIS) {
					if (nested == 0) {
						break;
					}
					nested--;
				}
				token_array_push(&in_parenthesis, array.tokens[i]);
			}
			if (nested != 0) {
				return 2;
			}
			double val;
			int err = calculate(in_parenthesis, &val);
			if (err) {
				return err;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.value.value = val,
				.position = pos,
			};
			token_array_push(&new_arr, number);
			free_token_array(in_parenthesis);
		}else if (array.tokens[i].token_type == TOKEN_RIGHT_PARENTHESIS) {
			return 2;
		}else{
			token_array_push(&new_arr, array.tokens[i]);
		}
	}
	*new_array = new_arr;
	return 0;
}

Error calculate_exponant(TokenArray array, TokenArray* new_array) {
	TokenArray new_arr = token_array_new();
	if (!new_arr.tokens) {
		return 1;
	}
	for (int i=0; i<array.number_of_tokens; i++) {
		if (array.tokens[i].token_type == TOKEN_EXPONANT) {
			i++;
			if ((new_arr.number_of_tokens-1) < 0 || i>= array.number_of_tokens || array.tokens[i].token_type != TOKEN_NUMBER) {
				return 2;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.position = i+1,
				.value.value = pow(new_arr.tokens[new_arr.number_of_tokens-1].value.value,array.tokens[i].value.value)
			};
			new_arr.number_of_tokens--;
			token_array_push(&new_arr, number);
		}else {
			token_array_push(&new_arr, array.tokens[i]);
		}
	}
	*new_array = new_arr;
	return 0;
}

Error calculate_multiplication_or_division(TokenArray array, TokenArray* new_array) {
	TokenArray new_arr = token_array_new();
	if (!new_arr.tokens) {
		return 1;
	}
	for (int i=0; i<array.number_of_tokens; i++) {
		if (array.tokens[i].token_type == TOKEN_MULTIPLICATION) {
			i++;
			if ((new_arr.number_of_tokens-1) < 0 || i>= array.number_of_tokens || array.tokens[i].token_type != TOKEN_NUMBER) {
				return 2;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.position = i+1,
				.value.value = new_arr.tokens[new_arr.number_of_tokens-1].value.value*array.tokens[i].value.value
			};
			new_arr.number_of_tokens--;
			token_array_push(&new_arr, number);
		}else if(array.tokens[i].token_type == TOKEN_DIVISION) {
			i++;
			if ((new_arr.number_of_tokens-1) < 0 || i>= array.number_of_tokens || array.tokens[i].token_type != TOKEN_NUMBER) {
				return 2;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.position = i+1,
				.value.value = new_arr.tokens[new_arr.number_of_tokens-1].value.value/array.tokens[i].value.value
			};
			new_arr.number_of_tokens--;
			token_array_push(&new_arr, number);
		}else {
			token_array_push(&new_arr, array.tokens[i]);
		}
	}
	*new_array = new_arr;
	return 0;
}

Error calculate_addition_or_substrction(TokenArray array, TokenArray* new_array) {
	TokenArray new_arr = token_array_new();
	if (!new_arr.tokens) {
		return 1;
	}
	for (int i=0; i<array.number_of_tokens; i++) {
		if (array.tokens[i].token_type == TOKEN_PLUS) {
			i++;
			if ((new_arr.number_of_tokens-1) < 0 || i>= array.number_of_tokens || array.tokens[i].token_type != TOKEN_NUMBER) {
				return 2;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.position = i+1,
				.value.value = new_arr.tokens[new_arr.number_of_tokens-1].value.value+array.tokens[i].value.value
			};
			new_arr.number_of_tokens--;
			token_array_push(&new_arr, number);
		}else if(array.tokens[i].token_type == TOKEN_MINUS) {
			i++;
			if ((new_arr.number_of_tokens-1) < 0 || i>= array.number_of_tokens || array.tokens[i].token_type != TOKEN_NUMBER) {
				return 2;
			}
			Token number = {
				.token_type = TOKEN_NUMBER,
				.position = i+1,
				.value.value = new_arr.tokens[new_arr.number_of_tokens-1].value.value-array.tokens[i].value.value
			};
			new_arr.number_of_tokens--;
			token_array_push(&new_arr, number);
		}else {
			token_array_push(&new_arr, array.tokens[i]);
		}
		// print_tokens(new_arr);
	}
	*new_array = new_arr;
	return 0;
}

// 0 if works
 Error calculate(TokenArray array, double* value) {
	// check for parenthesis
	TokenArray parenthesis_tokens;
	TokenArray exponant_tokens;
	TokenArray multiplication_tokens;
	TokenArray addition_tokens;
	int err;
	// printf("DEBUG\n\n");
	// print_tokens(array);
	// printf("DEBUG\n\n");
	err = calculate_parenthesis(array, &parenthesis_tokens);
	if (err) {
		return err;
	}
	// printf("DEBUG\n\n");
	// print_tokens(parenthesis_tokens);
	// printf("DEBUG\n\n");
	err = calculate_exponant(parenthesis_tokens, &exponant_tokens);
	if (err) {
		return err;
	}
	// printf("DEBUG\n");
	// print_tokens(multiplication_tokens);
	// printf("DEBUG\n");
	err = calculate_multiplication_or_division(exponant_tokens, &multiplication_tokens);
	if (err) {
		return err;
	}
	// printf("DEBUG\n");
	// print_tokens(multiplication_tokens);
	// printf("DEBUG\n");
	err = calculate_addition_or_substrction(multiplication_tokens, &addition_tokens);
	if (err) {
		return err;
	}
	// printf("DEBUG\n");
	// print_tokens(addition_tokens);
	// printf("DEBUG\n");
	assert(addition_tokens.number_of_tokens == 1);
	assert(addition_tokens.tokens[0].token_type == TOKEN_NUMBER);
	*value = addition_tokens.tokens[0].value.value;
	free_token_array(parenthesis_tokens);
	free_token_array(exponant_tokens);
	free_token_array(multiplication_tokens);
	free_token_array(addition_tokens);
	return 0;
}  

int main(int argc, char *argv[]){
	while (1) {
		char string[128] = {0};
		printf("> ");
		char c = 0;
		int i = 0;
		for (i=0; i<128 && (string[i]=fgetc(stdin)) != '\n' && string[i]!=EOF; i++);
		string[i] = 0;
		if (string[0] == 0) {
			break;
		}
		TokenArray array = tokenize_expression(string, strnlen(string, 128));
		double value = 0;
		int err = calculate(array, &value);
		switch (err) {
			case 0:
				printf("answer = %f\n", value);
			break;
			case 1:
				printf("malloc error\n");
			break;
			case 2:
				printf("synatax error\n");
			break;
		}
		free_token_array(array);
	}
	putchar('\n');
	return 0;
}
