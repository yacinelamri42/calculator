#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "error_handling.h"
#include "token.h"

TokenArray token_array_new(void) {
	TokenArray array = {
		.tokens = calloc(TOKEN_ALLOC_CHUNK, sizeof(Token)),
		.number_of_tokens = 0,
		.allocated_size_of_tokens = TOKEN_ALLOC_CHUNK,
	};
	if (!array.tokens) {
		PANIC("Could not allocate array");
	}
	return array;
}

void token_array_push(TokenArray* array, Token token) {
	if (array->allocated_size_of_tokens == array->number_of_tokens) {
		Token* t = realloc(array->tokens, (array->number_of_tokens+TOKEN_ALLOC_CHUNK)*sizeof(Token));
		if (!t) {
			PANIC("Could not reallocate array");
		}
		array->tokens = t;
		array->allocated_size_of_tokens+=TOKEN_ALLOC_CHUNK;
	}
	array->tokens[array->number_of_tokens++] = token;
}

TokenArray tokenize_expression(const char* expression, size_t string_length) {
	TokenArray token_array = {
		.tokens = NULL,
		.number_of_tokens=0,
		.allocated_size_of_tokens=0,
	};
	token_array.tokens = calloc(TOKEN_ALLOC_CHUNK, sizeof(Token));
	if (!token_array.tokens) {
		PANIC("Could not allocate array");
	}
	token_array.allocated_size_of_tokens = TOKEN_ALLOC_CHUNK;
	for (int i=0; i<string_length; ) {
		#ifdef DEBUG
		printf("token_array.tokens = %p\n", token_array.tokens);
		printf("token_array.number_of_tokens = %ld\n", token_array.number_of_tokens);
		printf("token_array.allocated_size_of_tokens = %ld\n", token_array.allocated_size_of_tokens);
		#endif /* ifdef DEBUG */
		if (expression[i] >= '0' && expression[i] <= '9') {
			int pos = i;
			char buffer[12] = {0};
			int buffer_index = 0;
			do {
				buffer[buffer_index++] = expression[i];
				i++;
			} while ((expression[i] >= '0' && expression[i] <= '9') || expression[i] == '.');
			buffer[buffer_index] = 0;
			double value = strtod(buffer, 0);
			Token tok = {
				.value.value = value,
				.position = pos,
				.token_type = TOKEN_NUMBER,
			};
			token_array_push(&token_array, tok);
		}else if (expression[i] == '(') {
			Token tok = {
				.value.operator = '(',
				.position = i,
				.token_type = TOKEN_LEFT_PARENTHESIS,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == ')') {
			Token tok = {
				.value.operator = ')',
				.position = i,
				.token_type = TOKEN_RIGHT_PARENTHESIS,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == '+') {
			Token tok = {
				.value.operator = '+',
				.position = i,
				.token_type = TOKEN_PLUS,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == '-') {
			Token tok = {
				.value.operator = '-',
				.position = i,
				.token_type = TOKEN_MINUS,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == '*') {
			Token tok = {
				.value.operator = '*',
				.position = i,
				.token_type = TOKEN_MULTIPLICATION,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == '/') {
			Token tok = {
				.value.operator = '/',
				.position = i,
				.token_type = TOKEN_DIVISION,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == '^') {
			Token tok = {
				.value.operator = '^',
				.position = i,
				.token_type = TOKEN_EXPONANT,
			};
			token_array_push(&token_array, tok);
			i++;
		}else if (expression[i] == ' ') {
			i++;
		}else if (expression[i] == 0 || expression[i] == EOF) {
			Token tok = {
				.value.operator = expression[i],
				.position = i,
				.token_type = TOKEN_END,
			};
			token_array_push(&token_array, tok);
			i++;
		}else {
			Token tok = {
				.value.operator = expression[i],
				.position = i,
				.token_type = TOKEN_UNKNOWN,
			};
			token_array_push(&token_array, tok);
			i++;
		}
	}
	return token_array;
}

void print_token(Token token) {
	switch (token.token_type) {
		case TOKEN_EXPONANT:
			printf("Token type: TOKEN_EXPONANT\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_PLUS:
			printf("Token type: TOKEN_PLUS\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_MINUS:
			printf("Token type: TOKEN_MINUS\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_UNKNOWN:
			printf("Token type: TOKEN_UNKNOWN\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_DIVISION:
			printf("Token type: TOKEN_DIVISION\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_MULTIPLICATION:
			printf("Token type: TOKEN_MULTIPLICATION\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_LEFT_PARENTHESIS:
			printf("Token type: TOKEN_LEFT_PARENTHESIS\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_RIGHT_PARENTHESIS:
			printf("Token type: TOKEN_RIGHT_PARENTHESIS\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_NUMBER:
			printf("Token type: TOKEN_NUMBER\n");
			printf("Token content: %f\n", token.value.value);
			printf("Token position: %ld\n", token.position);
		break;
		case TOKEN_END:
			printf("Token type: TOKEN_END\n");
			printf("Token content: %c\n", token.value.operator);
			printf("Token position: %ld\n", token.position);
		break;
	}
}

void print_tokens(TokenArray array) {
	for (int i=0; i<array.number_of_tokens; i++) {
		print_token(array.tokens[i]);
		printf("\n\n");
	}
}

void free_token_array(TokenArray array) {
	free(array.tokens);
}
