#ifndef TOKEN__H
#define TOKEN__H

#include <stddef.h>

typedef enum {
	TOKEN_EXPONANT,
	TOKEN_END,
	TOKEN_LEFT_PARENTHESIS,
	TOKEN_RIGHT_PARENTHESIS,
	TOKEN_NUMBER,
	TOKEN_PLUS,
	TOKEN_MINUS,
	TOKEN_MULTIPLICATION,
	TOKEN_DIVISION,
	TOKEN_UNKNOWN
} TokenType;

typedef struct {
	TokenType token_type;
	union {
		char operator;
		double value;
	} value;
	size_t position;
} Token;

#define TOKEN_ALLOC_CHUNK 16

typedef struct {
	Token* tokens;
	size_t number_of_tokens;
	size_t allocated_size_of_tokens;
} TokenArray ;

TokenArray token_array_new(void);
void token_array_push(TokenArray* array, Token token);
TokenArray tokenize_expression(const char* expression, size_t string_length);
void print_tokens(TokenArray array);
void print_token(Token token);
void free_token_array(TokenArray array);

#endif
