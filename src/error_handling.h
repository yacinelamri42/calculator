#ifndef ERROR_HANDLING__H
#define ERROR_HANDLING__H

#define SUPPORT_PANIC
#define SUPPORT_WARN
#define SUPPORT_LOG

#ifdef SUPPORT_PANIC
#include <stdio.h>
#include <stdlib.h>
#define PANIC(error_message, ...)                                                     \
	fprintf(stderr, "[PANIC at line %d in file %s]:\n\t", __LINE__, __FILE__);    \
	fprintf(stderr, error_message, ##__VA_ARGS__);                                \
	exit(1);                                                                      
#endif

#ifdef SUPPORT_WARN
#include <stdio.h>
#include <stdlib.h>
#define WARN(warning_message, ...)                                                   \
	fprintf(stderr, "[WARNING at line %d in file %s]:\n\t", __LINE__, __FILE__); \
	fprintf(stderr, warning_message, ##__VA_ARGS__);                             
#endif

#ifdef SUPPORT_LOG
#include <stdio.h>
#include <stdlib.h>
#define LOG(log_message, ...)                                                        \
	fprintf(stderr, "[LOG at line %d in file %s]:\n\t", __LINE__, __FILE__);     \
	fprintf(stderr, warning_message, ##__VA_ARGS__);                             
#endif

#endif
